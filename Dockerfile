FROM node:10-alpine as build-step

RUN mkdir -p /test-karma

WORKDIR /test-karma

COPY package.json /test-karma

RUN npm install

COPY . /test-karma

RUN npm run build

FROM nginx:1.17.1-alpine
	
COPY --from=build-step /app/dist/book-list-app /usr/share/nginx/html